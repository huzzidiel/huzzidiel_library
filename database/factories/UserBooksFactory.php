<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use App\Book;
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Book::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->jobTitle,
        'author' => $faker->name,
        'status' => $faker->boolean,
        'published_date' => $faker->date(),
        'category_id' => rand(1,20)
    ];
});
