<?php

use Illuminate\Database\Seeder;
use App\User;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class,20)->create();
        User::create([
            'name' => 'Huzzidiel Harrison',
            'email' => 'admin@library.com',
            'password' => bcrypt('secret'),
            'api_token' => str_random(60),
            'remember_token' => str_random(10),
        ]);
    }
}
