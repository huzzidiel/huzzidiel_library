<?php

use Illuminate\Database\Seeder;
use App\UserBooks;
class UserBooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(UserBooks::class,20)->create();
    }
}
