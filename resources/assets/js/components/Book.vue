<ntemplate>
    <div class="row">
        <!-- modal small -->
        <div class="modal fade" id="smallmodal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="smallmodalLabel">Small Modal</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>
                            There are three species of zebras: the plains zebra, the mountain zebra and the Grévy's zebra. The plains zebra and the mountain
                            zebra belong to the subgenus Hippotigris, but Grévy's zebra is the sole species of subgenus Dolichohippus. The latter
                            resembles an ass, to which it is closely related, while the former two are more horse-like. All three belong to the
                            genus Equus, along with other living equids.
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary">Confirm</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal small -->

        <!-- detalle elemento-->
        <div class="col-12" v-if="statusShow">
            <div class="card text-black-50">
                <div class="card-header">
                    <div class="card-title">
                        <h3><i class="fa fa-bookmark mr-2"></i>Detalle libro</h3>
                    </div>
                </div>
                <div class="card-body">
                    <div class="shadow p-4 my-4">
                        <p class=""><strong>Categoría: </strong></p>
                        <p>{{modelBook.category.name}}</p>
                    </div>
                    <div class="shadow p-4 my-4">
                        <p class=""><strong>Nombre: </strong></p>
                        <p>{{modelBook.name}}</p>
                    </div>
                    <div class="shadow p-4 my-4">
                        <p class="h5"><strong>Autor: </strong></p>
                        <p>{{modelBook.author}}</p>
                    </div>
                    <div class="shadow p-4 my-4">
                        <p class="h5"><strong>Estado: </strong></p>
                        <p>{{modelBook.status === 1 ? 'Dispobible' : 'No disponible'}}</p>
                    </div>
                    <div class="shadow p-4 my-4">
                        <p class="h5"><strong>Fecha publicaci[on: </strong></p>
                        <p>{{modelBook.published_date}}</p>
                    </div>
                    <div class="shadow p-4 my-4">
                        <p class="h5"><strong>Fecha creación: </strong></p>
                        <p>{{modelBook.created_at}}</p>
                    </div>
                    <div class="shadow p-4 my-4">
                        <p class="h5"><strong>Fecha actualización: </strong></p>
                        <p>{{modelBook.updated_at}}</p>
                    </div>
                    <div class="my-5 text-center text-lg-right">
                        <button class="btn btn-default" @click="show(modelBook)"><i class="fa fa-times mr-2"></i>Cerrar</button>
                        <button class="btn btn-primary" @click="edit(modelBook)"><i class="fa fa-edit mr-2"></i>Editar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end detalle elemento-->


        <!--formulario Registrar - Actualizar-->
        <div class="col-12" v-if="statusForm">
            <form v-on:submit.prevent="">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <h3><i class="fa fa-book mr-2"></i>{{statusUpdate === true ? 'Actualizar Libro' : 'Nuevo libro'}}</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-lg-8 offset-lg-2">
                                <div class="form-group">
                                    <label for="category">Categoría (<i class="fa fa-asterisk"></i>)</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-bookmark"></i>
                                        </div>
                                        <select id="category"
                                                class="form-control"
                                                name="category"
                                                v-model="modelBook.category_id"
                                                v-validate="'required'">
                                            <option v-for="item in listCategories"  v-bind:value="item.id">{{item.name}}</option>
                                        </select>
                                    </div>
                                    <span id="category_id-error"  class="mt-1 text-danger">{{ errors.first('category') }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-lg-8 offset-lg-2">
                                <div class="form-group">
                                    <label for="name">Nombre (<i class="fa fa-asterisk"></i>)</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-book"></i>
                                        </div>
                                        <input id="name"
                                               class="form-control"
                                               name="name"
                                               type="text"
                                               required
                                               minlength="4"
                                               maxlength="120"
                                               placeholder="Ej. Una botella en el Mar de Gaza"
                                               v-model="modelBook.name"
                                               v-validate="'required|alpha_spaces|min:4|max:120'">
                                    </div>
                                </div>
                                <span id="name-error" class="mt-1 text-danger">{{ errors.first('name') }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-lg-8 offset-lg-2">
                                <div class="form-group">
                                    <label for="author">Autor (<i class="fa fa-asterisk"></i>)</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <input id="author"
                                               class="form-control"
                                               name="author"
                                               type="text"
                                               required
                                               minlength="4"
                                               maxlength="120"
                                               placeholder="Ej.  Valérie Zenatti"
                                               v-model="modelBook.author"
                                               v-validate="'required|alpha_spaces|min:4|max:120'">
                                    </div>
                                </div>
                                <span id="author-error" class="mt-1 text-danger">{{ errors.first('author') }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-lg-4 offset-lg-2">
                                <div class="form-group">
                                    <label for="status">Estado (<i class="fa fa-asterisk"></i>)</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-adjust"></i>
                                        </div>
                                        <select id="status"
                                                class="form-control"
                                                name="status"
                                                v-model="modelBook.status"
                                                v-validate="'required'">
                                            <option value="1" >Disponible</option>
                                            <option value="1">No disponible</option>
                                        </select>
                                    </div>
                                </div>
                                <span id="status-error" class="mt-1 text-danger">{{ errors.first('status') }}</span>
                            </div>
                            <div class="col-12 col-lg-4">
                                <div class="form-group">
                                    <label for="published-date">Fecha de publicación (<i
                                            class="fa fa-asterisk"></i>)</label>
                                    <div class="input-group">
                                        <datetime v-model="modelBook.published_date"
                                                  title="Fecha de publicación"
                                                  class="form-control"></datetime>
                                        <!--<input id="published-date"-->
                                        <!--class="form-control"-->
                                        <!--name="published_date"-->
                                        <!--type="date"-->
                                        <!--v-model="modelBook.published_date"-->
                                        <!--v-validate="'required|date_format:dd/MM/yyyy'">-->

                                    </div>
                                </div>
                                <span id="published-date-error" class="mt-1 text-danger">{{ errors.first('published_date') }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-lg-8 offset-lg-2">
                                <div class="text-center text-lg-right">
                                    <button class="btn btn-default my-2" type="button" @click="create"><i
                                            class="fa fa-times mr-2"></i>Cancelar
                                    </button>
                                    <button class="btn btn-primary my-2" type="submit" v-if="statusUpdate === true"
                                            @click="update"><i class="fa fa-save mr-2"></i>Actualizar
                                    </button>
                                    <button class="btn btn-primary my-2" type="submit" v-if="statusUpdate === false"
                                            @click="store"><i class="fa fa-save mr-2"></i>Guardar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!--formulario Registrar - Actualizar-->

        <!-- listado - tabla-->
        <div class="col-md-12" v-if="statusList">
            <!-- DATA TABLE -->
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <div class="row">
                            <div class="col-12 col-sm-6 text-center text-sm-left">
                                <h3 class="text-uppercase"><i class="fa fa-book mr-2"></i>Libros</h3>
                            </div>

                            <div class="col-12 col-sm-6 text-center text-sm-left">
                                <div class="float-sm-right ">
                                    <button class="mt-4 au-btn au-btn-icon au-btn--green au-btn--small" @click="create">
                                        <i class="fa fa-plus-circle mr-2"></i>Nuevo Libro
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="float-lg-right">
                        <form class="form-header" v-on:submit.prevent="">
                            <input class="au-input au-input--xl" type="text" name="search"
                                   placeholder="Buscar..." v-model="search"/>
                            <button class="au-btn--submit" type="submit" :disabled="!search" @click="searchData">
                                <i class="zmdi zmdi-search"></i>
                            </button>
                            <button class="au-btn--submit" type="button" v-if="search" @click="index">
                                <i class="zmdi zmdi-brush"></i>
                            </button>
                        </form>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-data2">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Autor</th>
                                <th>Categoría</th>
                                <th>Estado</th>
                                <th>Fecha</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="(item, index) in listBooks.data" :key="item.id">
                                <td>{{index + 1}}</td>
                                <td><a href="#" @click="show(item)">{{item.name}}</a></td>
                                <td>{{item.author}}</td>
                                <td>{{item.category.name}}</td>
                                <td>
                                    {{item.status === 1 ? 'Dispobible' : 'No disponible'}}

                                </td>
                                <td>{{item.published_date}}</td>
                                <td>
                                    <div class="table-data-feature">
                                        <button class="item" data-toggle="tooltip" data-placement="top" title="Ver más"
                                                @click="show(item)">
                                            <i class="zmdi zmdi-eye"></i>
                                        </button>
                                        <button class="item" data-toggle="tooltip" data-placement="top" title="Editar"
                                                @click="edit(item)">
                                            <i class="zmdi zmdi-edit"></i>
                                        </button>
                                        <button class="item" data-toggle="tooltip" data-placement="top" title="Eliminar"
                                                @click="destroy(item)">
                                            <i class="zmdi zmdi-delete"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            <tr class="spacer"></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    <pagination :data="listBooks" @pagination-change-page="index"></pagination>
                </div>
            </div>
            <!-- END DATA TABLE -->
        </div>
        <!-- end listado - tabla-->

    </div>
</template>

<script>
    export default {
        data() {
            return {
                //Objeto que obtiene una lista de datos del servidor hacia el cliente
                listBooks: {},
                //Objeto que obtiene una lista de datos del servidor hacia el cliente
                listCategories: {},
                //Objeto que enlaza datos cliente a servidor
                modelBook: {
                    status: 1,
                    published_date: new Date().toISOString()
                },
                //false: el formulario dibuja boton guardar, true: el formarulario dibuja el boton actualizar
                statusUpdate: false,
                //default true: muesta el listado de datos
                statusList: true,
                //default false, en true: muesta el formulario para registrar o actualizar
                statusForm: false,
                //default false, en true: muesta el detalle de un elemento
                statusShow: false,
                //item para busqueda
                search: null,
                token: null,
            }
        },
        methods: {
            clearInput() {
                this.statusShow = false;
                this.statusUpdate = false;
                this.modelBook.id = null;
                this.modelBook.category_id = 1;
                this.modelBook.name = null;
                this.modelBook.author = null;
            },
            create(){
                let me = this;
                me.statusShow = false;
                me.viewList();
                me.viewForm();
                me.clearInput();
                me.categories();
            },
            categories(){
                let me = this;
              axios.get('api/categories/names?api_token='+ me.token).then(response => {
                  me.listCategories = response.data;
              }).catch(error => {
                  console.log(error);
                  me.$toastr.error('Consultar con el admin', 'Error');
              });
            },
            edit(book) {
                this.categories();
                this.viewForm();
                this.statusUpdate = true;
                this.statusShow = false;
                this.statusList = false;
                this.modelBook.id = book.id;
                this.modelBook.category_id = book.category_id;
                this.modelBook.name = book.name;
                this.modelBook.author = book.author;
                this.modelBook.status = book.status;
                this.modelBook.published_date = book.published_date;
            },
            index(page = 1) {
                let me = this;
                axios.get('api/boolistBooksks/index?api_token=' + me.token + "&page="+ page).then(response => {
                    me.listBooks = response.data;
                }).catch(errors => {
                    me.$toastr.error('Consultar con el admin', 'Error');
                    console.log(errors)
                });
            },
            viewForm() {
                this.statusForm = !this.statusForm;
            },
            show(book) {
                let me = this;
                me.viewShow();
                me.viewList();
                me.modelBook.id = book.id;
                me.modelBook.name = book.name;
                me.modelBook.author = book.author;
                me.modelBook.status = book.status;
                me.modelBook.published_date = book.published_date;
                me.modelBook.created_at = book.created_at;
                me.modelBook.updated_at = book.updated_at;
                me.modelBook.category = book.category;
            },
            searchData(){
                let me = this;
                axios.get('api/books/search?value=' + this.search + '&api_token=' + me.token).then(response => {
                    me.listBooks = response;
                }).catch(error => {
                    console.log(error);
                })
            },
            store() {
                let me = this;
                this.$validator.validateAll().then((result) => {
                    if (result) {
                        axios.post('api/books/store?api_token=' + me.token, me.modelBook).then(response => {
                            if (response.data.errors) {
                                me.$toastr.warning('Completa las validaciones', 'Validar campos');
                                $('#category_id-error').html(response.data.errors.category_id ? response.data.errors.category_id : "");
                                $('#name-error').html(response.data.errors.name ? response.data.errors.name : "");
                                $('#author-error').html(response.data.errors.author ? response.data.errors.author : "");
                                $('#status-error').html(response.data.errors.status ? response.data.errors.status : "");
                                $('#published-date-error').html(response.data.errors.published_date ? response.data.errors.published_date : "");
                            } else {
                                me.clearInput();
                                me.index();
                                me.viewList();
                                me.viewForm();
                                me.$toastr.success('Nuevo libro registrado', 'Libros');
                            }
                        }).catch(error => {
                            me.$toastr.error('Consultar con el admin', 'Error');
                            console.log(error);
                        })
                    } else {
                        me.$toastr.warning('Completa las validaciones', 'Validar campos');
                    }
                });
            },
            update() {
                let me = this;
                this.$validator.validateAll().then((result) => {
                    if (result) {
                        axios.put('api/books/update?api_token=' + me.token, me.modelBook).then(response => {
                            //No pasan validaciones en el servidor
                            if (response.data.errors) {
                                me.$toastr.warning('Completa las validaciones', 'Validar campos');
                                $('#name-error').html(response.data.errors.name ? response.data.errors.name : "");
                                $('#description-error').html(response.data.errors.description ? response.data.errors.description : "");
                            } else {
                                me.index();
                                me.viewForm();
                                me.viewList();
                                me.clearInput();
                                me.$toastr.success('Libro actualizado', 'Libros');
                            }
                        }).catch(error => {
                            me.$toastr.error('Consultar con el admin', 'Error');
                            console.log(error);
                        })
                    } else {
                        me.$toastr.warning('Completa las validaciones', 'Validar campos');
                    }
                });
            },
            destroy(book) {
                let me = this;
                me.$swal({
                    title: 'Libros',
                    html: "¿Esta seguro de eliminar el libro <strong>" + book.name + "</strong>?",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#AEAFAF',
                    confirmButtonText: 'Eliminar',
                    cancelButtonText: 'Cancelar',
                }).then(function (result) {
                    if (result.value === true) {
                        axios.delete('api/books/destroy?api_token=' + me.token+ "&id="+ book.id).then(response => {
                            if (response.data.status === true) {
                                me.index();
                                me.$toastr.success('Libro eliminado', 'Libros');
                            } else {
                                me.$toastr.error('Consultar con el admin', 'Error');
                            }
                        }).catch(error => {
                            me.$toastr.error('Consultar con el admin', 'Error');
                            console.log(error);
                        })
                    }
                }).catch(me.$swal.noop);
            },
            getToken() {
                let me = this;
                axios.get('home/api').then(response => {
                    if (response.data.token !== false) {
                        me.token = response.data.token;
                        this.index();
                    }
                }).catch(errors => {
                    console.log(errors);
                })
            },

            //------------- METODOS VISTAS -------------
            // Metodos para mostrar/ocultar vistas, cambian une estado de verdadero o falso
            // permitiendo ocultar u ocultar vistas
            viewShow(){
                this.statusShow = !this.statusShow;
            },
            viewList(){
                this.statusList = !this.statusList;
            },
            viewForm(){
                this.statusForm = !this.statusForm;
            },
        }
        ,
        mounted() {
            this.getToken();
        }
    }
</script>
