
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import Vue from 'vue'

//notifications
import VueToastr2 from 'vue-toastr-2'
import 'vue-toastr-2/dist/vue-toastr-2.min.css'

//sweet (confirm delete item)
import VueSweetalert2 from 'vue-sweetalert2';

//validations
import VeeValidate from 'vee-validate';

//datetime
import Datetime from 'vue-datetime'
import 'vue-datetime/dist/vue-datetime.css'

//axios
import axios from 'axios'
import VueAxios from 'vue-axios'

//notifications
window.toastr = require('toastr');
window.Vue = require('vue');
Vue.use(VueToastr2)

//sweet (confirm delete item)
Vue.use(VueSweetalert2);

//datetime
Vue.use(Datetime)

//axios
Vue.use(VueAxios, axios)


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));
Vue.component('dashboard', require('./components/Dashboard.vue').default);
Vue.component('category', require('./components/Category.vue').default);
Vue.component('book', require('./components/Book.vue').default);
Vue.component('user', require('./components/User.vue').default);
Vue.component('land', require('./components/Loan.vue').default);

//paginacion
Vue.component('pagination', require('laravel-vue-pagination'));



//Vee validation: Validaciones en idioma espaniol
var VueValidation = require('vee-validate');
var VueValidationSwe = require('vee-validate/dist/locale/es');
Vue.use(VueValidation, {
    locale: 'es',
    dictionary: {
        es: VueValidationSwe
    }
});


const app = new Vue({
    el: '#app',
    data:{
        menu:0
    }
});
