<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Librery test example">
    <meta name="author" content="Huzzidiel Harrison ">
    <meta name="csrf-token" content="{{ csrf_token() }}">



    <!-- Title Page-->
    <title>Librery</title>

    <!-- Fontfaces CSS-->
    <link href="{{asset('css/font-face.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('css/font-awesome-4.7/css/font-awesome.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('css/font-awesome-5/css/fontawesome-all.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('css/mdi-font/css/material-design-iconic-font.min.css')}}" rel="stylesheet" media="all">
    <link rel="shortcut icon" href="{{asset('images/icon/favicon.png')}}" type="image/x-icon">


    <!-- Bootstrap CSS-->
    <link href="{{asset('css/bootstrap-4.1/bootstrap.min.css')}}" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="{{asset('css/animsition/animsition.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('css/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet"
          media="all">
    <link href="{{asset('css/wow/animate.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('css/css-hamburgers/hamburgers.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('css/slick/slick.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('css/select2/select2.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('css/perfect-scrollbar/perfect-scrollbar.css')}}" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="{{asset('css/theme.css')}}" rel="stylesheet" media="all">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>

<body class="animsition">
<div id="app" class="page-wrapper">
    <!-- Collapsed Hamburger -->

    <!-- MENU SIDEBAR-->
    <aside class="menu-sidebar d-none d-lg-block">
        <div class="logo">
            <a href="#" @click="menu=0">
                <img src="{{asset('images/icon/logo.png')}}" alt="Librery"/>
            </a>
        </div>
        <div class="menu-sidebar__content js-scrollbar1">
            <nav class="navbar-sidebar">
                <ul class="list-unstyled navbar__list">
                    <li class="active has-sub">
                        <a href="#" @click="menu=0">
                            <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                    </li>
                    <li>
                        <a href="#" @click="menu=1">
                            <i class="fas fa-cubes"></i>Categorías</a>
                    </li>
                    <li>
                        <a href="#" @click="menu=2">
                            <i class="fas fa-book"></i>Libros</a>
                    </li>
                    <li>
                        <a href="#" @click="menu=3">
                            <i class="far fa-user"></i>Usuarios</a>
                    </li>
                    <li>
                        <a href="#" @click="menu=4">
                            <i class="fa fa-archive"></i>Prestamos</a>
                    </li>
                </ul>
            </nav>
        </div>
    </aside>
    <!-- END MENU SIDEBAR-->


    <!-- PAGE CONTAINER-->
    <div class="page-container">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="float-sm-left visible-xs visible-sm">
                                <div class="dropdown text-center">
                                    <button class="btn dropdown-toggle" type="button"
                                            id="dropdownMenuButton"
                                            data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">Menu
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#" @click="menu=0">
                                            <i class="fas fa-tachometer-alt mr-2"></i>Dashboard</a>
                                        <a class="dropdown-item" href="#" @click="menu=1">
                                            <i class="fas fa-cubes mr-2"></i>Categorías</a>
                                        <a class="dropdown-item" href="#" @click="menu=2">
                                            <i class="fas fa-book mr-2"></i>Libros</a>
                                        <a class="dropdown-item" href="#" @click="menu=3">
                                            <i class="far fa-user mr-2"></i>Usuarios</a>
                                        <a class="dropdown-item" href="#" @click="menu=4">
                                            <i class="fa fa-archive mr-2"></i>Prestamos</a>
                                        <a class="dropdown-item" href="{{ route('closeSession') }}">
                                            <i class="zmdi zmdi-power mr-2"></i>Cerrar sesión
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="float-sm-right">
                                <div class="dropdown hidden-xs">
                                    <button class="btn dropdown-toggle" type="button"
                                            id="dropdownMenuButton"
                                            data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">
                                        {{ Auth::user()->name }}
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="{{ route('closeSession') }}">
                                            <i class="zmdi zmdi-power mr-2"></i>Cerrar sesión
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- HEADER DESKTOP-->

        <!-- MAIN CONTENT-->
        <div class="main-content">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <dashboard v-if="menu === 0"></dashboard>
                    <category v-if="menu === 1"></category>
                    <book v-if="menu === 2"></book>
                    <user v-if="menu === 3"></user>
                    <land v-if="menu === 4"></land>
                    {{--footer--}}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <p>Copyright © 2019 Library. All rights reserved. by <a
                                            href=http://huzzidielharrison.com/portafolio>Huzzidiel</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>
</div>


<!-- Jquery JS-->
<script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
<!-- Bootstrap JS-->
<script src="{{asset('js/bootstrap-4.1/popper.min.js')}}"></script>
<script src="{{asset('js/bootstrap-4.1/bootstrap.min.js')}}"></script>

<!-- Vendor JS       -->
<script src="{{asset('js/slick/slick.min.js')}}">
</script>
<script src="{{asset('js/wow/wow.min.js')}}"></script>
<script src="{{asset('js/animsition/animsition.min.js')}}"></script>
<script src="{{asset('js/bootstrap-progressbar/bootstrap-progressbar.min.js')}}">
</script>
<script src="{{asset('js/counter-up/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('js/counter-up/jquery.counterup.min.js')}}"></script>
<script src="{{asset('js/circle-progress/circle-progress.min.js')}}"></script>
<script src="{{asset('js/perfect-scrollbar/perfect-scrollbar.js')}}"></script>
<script src="{{asset('js/chartjs/Chart.bundle.min.js')}}"></script>
<script src="{{asset('js/select2/select2.min.js')}}"></script>

<!-- Main JS-->
<script src="{{asset('js/main.js')}}"></script>
<script src="{{asset('js/app.js')}}"></script>

</body>

</html>
<!-- end document-->