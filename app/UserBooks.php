<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBooks extends Model
{
    protected $table = 'user_books';
    protected $fillable = [
        'user_id',
        'book_id',
        'start_date',
        'end_date',
        'user_id',
        'book_id',
    ];
    
    public function user(){
        return $this->hasOne(User::class, 'id','user_id');
    }

    public function book(){
        return $this->hasOne(Book::class,'id','book_id');
    }



}
