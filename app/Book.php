<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = 'books';
    protected $fillable = [
        'name',
        'author',
        'status',
        'published_date',
        'category_id',
    ];

    public function category(){
        return $this->hasOne(Category::class,'id','category_id');
    }

    public function user(){
        return $this->belongsToMany('\App\User','user_books')
            ->withPivot('user_id');
    }
    public function loans()
    {
        return $this->hasMany(UserBooks::class,'book_id','id');
    }
}
