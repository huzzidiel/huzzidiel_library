<?php

namespace App\Http\Controllers;

use App\Book;
use App\Http\Requests\GeneralValidation;
use App\UserBooks;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class BookController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        $books = Book::orderBy('id','desc')->with('category')->paginate(5);
        return response()->json($books);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function names(){
        $books = Book::select('id','name')->where('status',true)->get();
        return response()->json($books);
    }



    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request){
        $rules = [
            'category_id' => [
              'required'
            ],
            'name' => [
                'required',
                'regex:(^(' . GeneralValidation::texto() . ')$)',
                'min:4',
                'max:120'
            ],
            'author' => [
                'required',
                'regex:(^(' . GeneralValidation::texto() . ')$)',
                'min:4',
                'max:120'
            ],
            'status' => [
                'required',
                'boolean'
            ],
            'published_date' => [
                'required',
                'date'
            ],
        ];
        $validator = Validator::make($request->all(),$rules );
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors() ]);
        }
        $publishDate = Carbon::parse($request->published_date);
        $request['published_date'] = $publishDate->format('Y-m-d');
        $book = new Book();
        $book->fill($request->all());
        if($book->save()){
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }

    public function update(Request $request){
        $rules = [
            'category_id' => [
                'required'
            ],
            'name' => [
                'required',
                'regex:(^(' . GeneralValidation::texto() . ')$)',
                'min:4',
                'max:120'
            ],
            'author' => [
                'required',
                'regex:(^(' . GeneralValidation::texto() . ')$)',
                'min:4',
                'max:120'
            ],
            'status' => [
                'required',
                'boolean'
            ],
            'published_date' => [
                'required',
                'date'
            ],
        ];
        $validator = Validator::make($request->all(),$rules );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors() ]);
        }
        $publishDate = Carbon::parse($request->published_date);
        $request['published_date'] = $publishDate->format('Y-m-d');;
        $book = Book::find($request->id);
        $book->fill($request->all());

        if($book->save()){
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }

    public function search(Request $request){
        $books = Book::where('name','like','%'.$request->value.'%')
            ->with('category')
            ->orWhere('author','like','%'.$request->value.'%')
            ->get();
        return response()->json($books);
    }


    public function destroy(Request $request){
        $book = Book::find($request->id);
        $bookLoans = $book->loans;
        foreach ($bookLoans as $lean){
            $lean->delete();
        }
        $book =  Book::destroy($request->id);
        if($book){
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }


    public function total(){
        return response()->json(Book::count());
    }


}
