<?php

namespace App\Http\Controllers;

use App\Book;
use App\User;
use App\UserBooks;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LoanController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $loans = UserBooks::orderBy('id', 'desc')->with('user')->with('book')->paginate(5);
        return response()->json($loans);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {

        $rules = [
            'user_id' => [
                'required',
            ],
            'book_id' => [
                'required',
            ],
            'start_date' => [
                'required',
                'date',
            ],
            'end_date' => [
                'required',
                'date',
            ],
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }
        $load = new UserBooks();
        $start_date = Carbon::parse($request->start_date);
        $end_date = Carbon::parse($request->end_date);
        $request['start_date'] = $start_date->format('Y-m-d');
        $request['end_date'] = $end_date->format('Y-m-d');
        $load->fill($request->all());
        if ($load->save()) {
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }


    public function search(Request $request)
    {
        $loans = collect();
        $users = User::where('name', 'like', '%' . $request->value . '%')->get();
        $books = Book::where('name', 'like', '%' . $request->value . '%')->get();
        foreach ($users as $user) {
            $loansQuery = UserBooks::where('user_id', $user->id)->with('user')->with('book')->get();
            foreach ($loansQuery as $loan) {
                $loans->push($loan);
            }
        }
        foreach ($books as $book) {
            $loansQuery = UserBooks::where('book_id', $book->id)->with('user')->with('book')->get();
            foreach ($loansQuery as $loan) {
                $loans->push($loan);
            }
        }
        return response()->json($loans);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {

        $rules = [
            'user_id' => [
                'required',
            ],
            'book_id' => [
                'required',
            ],
            'start_date' => [
                'required',
                'date',
            ],
            'end_date' => [
                'required',
                'date',
            ],
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }
        $load = UserBooks::find($request->id);
        $start_date = Carbon::parse($request->start_date);
        $end_date = Carbon::parse($request->end_date);
        $request['start_date'] = $start_date->format('Y-m-d');
        $request['end_date'] = $end_date->format('Y-m-d');
        $load->fill($request->all());

        if ($load->save()) {
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }


    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $load = UserBooks::destroy($request->id);
        if ($load) {
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }

    public function loadLastMonth()
    {
        $now = Carbon::now();
        $now->month;
        $totalLoadLastMonth  = UserBooks::whereYear('start_date','=',$now->year)->whereMonth('start_date', '=', $now->month)->count();
        return response()->json($totalLoadLastMonth);
    }

    public function loadLastYear()
    {
        $now = Carbon::now();
        $now->month;
        $totalLoadLastYear  = UserBooks::whereYear('start_date', '=', $now->year)->count();
        return response()->json($totalLoadLastYear);
    }
}
