<?php

namespace App\Http\Controllers;

use App\Http\Requests\GeneralValidation;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class UserController extends Controller
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        $users = User::orderBy('id','desc')->paginate(5);
        return response()->json($users);
    }





    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request){

        $rules = [
            'name' => [
                'required',
                'regex:(^(' . GeneralValidation::texto() . ')$)',
                'min:4',
                'max:120'
            ],
            'email' => [
                'required',
                'email',
                'min:4',
                'max:120'
            ],
        ];
        $validator = Validator::make($request->all(),$rules );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors() ]);
        }
        $user = new User();
        //password defautl secret
        $request['password'] = bcrypt('secret');
        $request['api_token'] = str_random(60);
        $request['remember_token'] = str_random(10);

        $user->fill($request->all());
        if($user->save()){
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }



    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request){
        $rules = [
            'name' => [
                'required',
                'regex:(^(' . GeneralValidation::texto() . ')$)',
                'min:4',
                'max:120'
            ],
            'email' => [
                'required',
                'email',
                'min:4',
                'max:120'
            ],
        ];
        $validator = Validator::make($request->all(),$rules );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors() ]);
        }
        $user = User::find($request->id);
        $user->fill($request->all());

        if($user->save()){
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }


    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request){
        $user = User::find($request->id);
        $userLoans = $user->loans;
        foreach ($userLoans as $lean){
            $lean->delete();
        }
        $user =  User::destroy($request->id);
        if($user){
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function names(){
        $categories = User::select('id','name')->get();
        return response()->json($categories);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request){
        $users = User::where('name','like','%'.$request->value.'%')
            ->orWhere('email','like','%'.$request->value.'%')
            ->get();
        return response()->json($users);
    }

    public function total(){
        return response()->json(User::count());
    }
}
