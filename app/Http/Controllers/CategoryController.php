<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\GeneralValidation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        $categories = Category::orderBy('id','desc')->paginate(5);
        return response()->json($categories);
    }

    public function names(){
        $categories = Category::select('id','name')->get();
        return response()->json($categories);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request){

        $rules = [
            'name' => [
                'required',
                'regex:(^(' . GeneralValidation::texto() . ')$)',
                'min:4',
                'max:120'
            ],
            'description' => [
                'required',
                'regex:(^(' . GeneralValidation::texto() . ')$)',
                'min:4',
                'max:120'
            ],
        ];
        $validator = Validator::make($request->all(),$rules );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors() ]);
        }
        $category = new Category();
        $category->fill($request->all());
        if($category->save()){
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id){

        $category = Category::find($id);
        return response()->json(['category' => $category]);
    }

    public function search(Request $request){
        $categories = Category::where('name','like','%'.$request->value.'%')
            ->orWhere('description','like','%'.$request->value.'%')
            ->get();
        return response()->json($categories);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request){
        $rules = [
            'name' => [
                'required',
                'regex:(^(' . GeneralValidation::texto() . ')$)',
                'min:4',
                'max:120'
            ],
            'description' => [
                'required',
                'regex:(^(' . GeneralValidation::texto() . ')$)',
                'min:4',
                'max:120'
            ],
        ];
        $validator = Validator::make($request->all(),$rules );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors() ]);
        }
        $category = Category::find($request->id);
        $category->fill($request->all());

        if($category->save()){
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }


    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request){
        $category = Category::find($request->id);
        $categoryBooks = $category->books;
        foreach ($categoryBooks as $book){
            $book->delete();
        }
        $category =  Category::destroy($request->id);
        if($category){
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }

}
