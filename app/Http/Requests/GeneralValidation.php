<?php
/**
 * Created by PhpStorm.
 * User: harriroot
 * Date: 16/06/19
 * Time: 18:58
 */

namespace App\Http\Requests;


class GeneralValidation
{
    public static function texto(){
        return '[\d\w\á\Á\é\É\í\Í\ó\Ó\ú\Ú\ü\Ü\ñ\Ñ\.\#\-\,\s]+';
    }

    public static function telefono()
    {
        return '[0-9]+';
    }
}