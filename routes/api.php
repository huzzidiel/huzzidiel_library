<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::group(['prefix' => 'categories', 'middleware' => 'auth:api'], function () {
    Route::get('index', 'CategoryController@index');
    Route::get('names', 'CategoryController@names');
    Route::get('search', 'CategoryController@search');
    Route::post('store', 'CategoryController@store');
    Route::put('update', 'CategoryController@update');
    Route::delete('destroy', 'CategoryController@destroy');
});

Route::group(['prefix' => 'books', 'middleware' => 'auth:api'], function () {
    Route::get('index', 'BookController@index');
    Route::get('names', 'BookController@names');
    Route::get('total', 'BookController@total');
    Route::get('search', 'BookController@search');
    Route::post('store', 'BookController@store');
    Route::put('update', 'BookController@update');
    Route::delete('destroy', 'BookController@destroy');
});

Route::group(['prefix' => 'users', 'middleware' => 'auth:api'], function () {
    Route::get('index', 'UserController@index');
    Route::get('names', 'UserController@names');
    Route::get('total', 'UserController@total');
    Route::get('search', 'UserController@search');
    Route::post('store', 'UserController@store');
    Route::put('update', 'UserController@update');
    Route::delete('destroy', 'UserController@destroy');
});

Route::group(['prefix' => 'loans', 'middleware' => 'auth:api'], function () {
    Route::get('index', 'LoanController@index');
    Route::get('names', 'LoanController@names');
    Route::get('loadLastMonth', 'LoanController@loadLastMonth');
    Route::get('loadLastYear', 'LoanController@loadLastYear');
    Route::get('search', 'LoanController@search');
    Route::post('store', 'LoanController@store');
    Route::put('update', 'LoanController@update');
    Route::delete('destroy', 'LoanController@destroy');
});
